package praktikum8;

public class Kuulujutud {

	public static void main(String[] args) {

		String[] naised = { "Mari", "Kristiina", "Kelli" };
		String[] mehed = { "Karl", "Helmut", "Aleks" };
		String[] tegusonad = { "magavad", "peksavad", "ignoreerivad" };

		int suvalinemees = (int) (Math.random() * mehed.length);
		int suvalinetegevus = (int) (Math.random() * tegusonad.length);
		int suvalinenaine = (int) (Math.random() * naised.length);

		System.out.println(mehed[suvalinemees] + " ja " + naised[suvalinenaine] + " " + tegusonad[suvalinetegevus]);

	}

	// public static String suvalineElement(String[] sonad) {
	//
	// int suvalineIndeks = 0 + (int)(Math.random() * naised.length);
	// return sonad[suvalineIndeks];
	// }

}
